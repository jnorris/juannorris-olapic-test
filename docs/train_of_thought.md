# Development Process of the Project

Detailed description about the train of thought, chosen tools and implementation
decisions.

During the entire process, I took notes, which are here translated in a more
formal manner.

## Getting Started

The first step was to thoroughly read the test description, do a quick research
about conversion funnels and write down the problem in paper, with a basic
example, to really understand the requirements and be able to diagram a possible
solution.

While clearing out some doubts about requirements with the team, I wrote down
objectives/steps:

1. Have a glance at the raw data to consider how to store it.
1. Decide the technology stack.
1. Solve the main problem in the query language of the chosen DB.
1. Create the repository.
1. Add a README file, as well as the test description.
1. Add linters and base config files.
1. Create the core component to setup/manage the database.
1. Create a script to parse and import the raw data.
1. Translate the solution into models and functions.
1. Actually test/verify that the solution works for the problem at hand.
1. Create the REST API component and the conversion funnel endpoint.
1. Add tests whenever possible.

Easter eggs (if there is enough time):

1. Given that it could be a good metric knowing which devices are the less prone
to get to the checkout step, implement an endpoint to retrieve such information.
1. Create a simple page that displays all existing clients, lets the user select
one, and draws the conversion funnel chart.

## Technology Stack

I chose to implement my solution in Python, using a stack that I think (from
experience in previous projects and reading articles) is reliable, fast,
scalable, robust and -IMHO also important- easy to understand and build upon.

* [PostgreSQL](www.postgresql.org.es/)
* Python
    * [Flask](flask.pocoo.org)
    * [Flask-RESTful](http://flask-restful.readthedocs.io/)
    * [webargs](https://webargs.readthedocs.io/)
    * [Flask-SQLAlchemy](flask-sqlalchemy.pocoo.org)

### Database

For the given problem and provided data, I think either of SQL and NoSQL
databases could have been used, but since there could be clearly defined
object-relations in the real application from where the data has been taken, and
I'm more used to SQL DBs, I chose PostreSQL.

As for the ORM, I used SQLAlchemy, with the Flask-SQLAlchemy extension, which
auto-magically maps models with DB tables and provides some helpers.

### Web Framework and tools

**Flask**

I like this web micro-framework because it's really lightweight, easy to
understand, use and extend, giving the whole control of the application to the
developer.

This allows to modify, upgrade and change entire components of the application,
removing the need to over engineer the solution, or pivot when the current
implementation doesn't cut it.

**Flask-RESTful**

It allows to quickly create REST APIs, by wrapping requests and adding common
bits, like content negotiation, error handling, request parameters parser, json
encoding, etc. It also allows to easily creat system CRUDs, from a class based
view.

**Request Parsing and Data Output**

It's been a couple of years since I've used *Flask-RESTful*, and I stumbled upon
a bug with *reqparse* -the request param parser-, so I researched about it.

Given that *reqparse* has many flaws and it will be removed from *Flask-RESTful*
in the future -as discussed [here](https://github.com/flask-restful/flask-restful/issues/335)-,
I switched to *webargs*, which relies in *marshmallow* -a great object
serialization/deserialization tool- and it doesn't require a lot of extra
effort.

In the past, I've also had to drop *marshal_with*, *Flask-RESTful*'s parser for
data output, in favor of *flask-marshmallow* -a marshmallow wrapper for Flask-,
which is really good to define model schemas, for both, input and output. I
decided not to include it for now though, since it's more helpful when used
directly with DB models, which is not needed for the purposes of this project.

I added a **CustomJSONEncoder** class, which wasn't really necessary now, but
it is a nice-to-have, since most projects eventually run into the problem of
having to encode objects outside the JSON specification (like date times).

I'm not entirely happy on how *Flask-RESTful* suggests to override the default
encoder class: importing application code into the config file.

I think config files should be kept as clean as possible, and should only care
about environment settings and/or settings that might change in time, like 3rd
party URLs, DB, SMPT, etc. The encoder isn't something that will change from one
environment to another, nor will it change in time (the implementation might,
but not the class name). An alternative, might be doing it before registering
the API blueprint:

```python
def init_app(app):
    restful_json = app.config.get('RESTFUL_JSON', {})
    restful_json.update(cls=CustomJSONEncoder)
    app.config.update({
        'RESTFUL_JSON': restful_json
    })
    app.register_blueprint(blueprint)
```

NOTE: By stating all this I'm not saying *Flask-RESTful* isn't good enough, it's
actually sufficient for this project, but it might be worth investigating if
there are better alternatives. I didn't have enough time to do so, but these
libraries seem interesting:

* [flask-apisec](https://github.com/jmcarp/flask-apispec)
* [flask-classful](https://github.com/teracyhq/flask-classful)


## Solving the Problem

After some iterations, this is the final query that should output the desired
metric:

```sql
-- Count each user only once per step. Steps are in sequence in the result, from
-- left to right
SELECT
  COUNT(DISTINCT step1.user_id) AS STEP1,
  COUNT(DISTINCT step2.user_id) AS STEP2,
  COUNT(DISTINCT step3.user_id) AS STEP3
FROM USER_HIT step1
  -- Make sure stepN is counted iff it has occurred after stepN-1 in the
  -- pre-defined time window:
  -- stepN-1.timestamp <= stepN.timestamp <= stepN-1.timestamp + TIME_WINDOW
  LEFT JOIN USER_HIT step2 ON
      step1.user_id = step2.user_id
      AND step2.timestamp BETWEEN step1.timestamp AND (step1.timestamp + :time_window)
      -- Select only events in step2
      AND step2.event_id IN (13, 14, 15)
      -- Date time range filter (date_from is ensured from previous step)
      AND step2.timestamp < :date_to
  LEFT JOIN USER_HIT step3 ON
      step3.user_id = step2.user_id
      AND step3.timestamp BETWEEN step2.timestamp AND (step2.timestamp + :time_window)
      AND step2.timestamp <= step3.timestamp
      -- Select only events in step3
      AND step3.event_id = 1
      -- Date time range filter (date_from is ensured from previous step)
      AND step3.timestamp < :date_to
WHERE
  -- Select only events in step1
  step1.event_id = 11
  -- Select rows only for the desired customer
  AND step1.customer_id = :customer_id
  -- Date time range filter
  AND :date_from <= step1.timestamp AND step1.timestamp < :date_to;
```

where

* Step 1: Widget View
* Step 2: Widget Interaction
* Step 3: Checkout

The query has optional filters:

* By date time range:
    * Get everything from **date_from**.
    * Get everything up to **date_to**.
    * Get everything between **date_from** and **date_to**.

The funnel is computed by taking into account a maximum time window between each
step, that is, the user needs to perform each step with no more than
'time_window' seconds between each other.

This query can be ran in the `psql` console, by first setting up the parameters.
For instance:

```sh
\set time_window 'INTERVAL \'1800 seconds\''
\set customer_id 216182
\set date_from '\'2015-10-03 20:28:45.919\''
\set date_to '\'2015-10-03 20:48:45.919\''
```

## Repository

As usual now days, I used Git for the repository.

The initial commit contained only the .gitignore file, which I generated using a
[gibo](https://github.com/simonwhitaker/gibo), a simple tool that I wanted to
share with you:

```sh
# Ignore files related to Python language/frameworks and JetBrains IDEs
gibo Python JetBrains > .gitignore
```

Then, I added the basic python requirements and project structure.

**Requirements**

Requirements are divided into 2 files:

* `requirements.txt`: application dependencies, needed to be able to run it.
* `requirements_dev.txt`: development dependencies, needed to run tests, linters
and any other tools helpful during developing the application.

**Project Structure**

The project has a python package at the root level, called `olapic_app`, which
is divided into 2 main components, the first one contains the data and business
layers, i.e., the `core` of the application, and the other one contains the
`web` application itself, with the REST API implementation.

The idea here is that both components should be as decoupled as possible. The
`web` component will of course depend on the `core`, but it should only use it
trough a defined interface (classes and functions), without knowing anything
about the actual DB implementation, which is `core`'s responsibility.

This aids in scalability and flexibility, making it possible, if needed, e.g.,
to separate each component in different repositories, as installable python
packages, or to change the entire implementation of the data layer with minimum
-to none- impact in the `web` package, and the other way around.

There is a more detailed explanation about the structure and its files
[here](./architecture.md#project-structure).


## Linters

The next step was adding and configuring linters, to run checks on the python
code against the style guide inspired in [PEP-0008](https://www.python.org/dev/peps/pep-0008/)
as well as static analysis.

I chose to use [pylint](https://pylint.readthedocs.io/en/latest/) and
[pycodestyle](http://pycodestyle.pycqa.org/en/latest/).

I think that, while developing, it is a good practice to run linters in the
local machine, before committing your code, which can be easily achieved using
a pre-commit hook.

For this project, I worked on a hook that would run both, pylint and
pycodestyle, and output all errors to the console:

```sh
#!/bin/bash

# Get python files to be commited (ignore deleted files)
FILES=$(echo -n `git diff --staged --name-only --diff-filter=d | grep -e '\.py$'`)

# Run linters if there are files to check
if [ ! -z "$FILES" ]; then

    # Create a temporary directory
    tmpdir=$(mktemp -d commitXXXXXX)
    trap "rm -rf $tmpdir" EXIT

    # Checkout the files to be committed into that directory
    git checkout-index --prefix=$tmpdir/ -af

    # Move to the temporary directory
    cd $tmpdir;

    # Run linters and capture return values
    pycodestyle $FILES;
    PEP8_RET=$?;

    pylint --reports=no $FILES;
    PYLINT_RET=$?;

    # Move back to the previous directory
    cd ..

    # Exist with code 1 if there were errors
    if [[ $PEP8_RET -ne 0  || $PYLINT_RET -ne 0 ]]; then
        exit 1
    fi
fi
```

This script can be enabled by pasting its contents in the
`.git/hooks/pre-commit` file of the repository.

NOTE: I didn't have time to try out
[pydocstyle](http://www.pydocstyle.org/en/latest/), but it looks interesting.

## Core

To model the problem I used only 1 table, called UserHit, which contains all
the information of a row in the raw data file.

For the purposes of this test, I didn't think it was necessary to add more than
one model (or table), but it is clear that more models would be needed to model
a 'real' application: at least the Customer and User models.

This becomes even clearer when thinking about extending the application. For
instance, if I were to implement the single page application to consume the
API, I'd like to query for all the costumers, which would take much less time
if there were a Customer model (with only a couple hundreds of rows) than
querying the UserHit model (with millions of rows).

The core component relies on *Flask-SQLAlchemy*, a thin wrapper of *SQLAlchemy*.

By adding this dependency, I'm adding some coupling with the `web` component,
because replacing *Flask* will mean to remove *Flask-SQLAlchemy*

I'm okay with this decision because I'm gaining a lot of helpers -all accessible
from within the `core.db` object- as well as saving many lines of code.

Further more, I'm confident *Flask* is a good choice, that can scale and I won't
need to drop in the future. IMHO, it is more probable to change the data layer
rather than the web framework.

## Raw Data Importer

Once all that was in place, I concentrated my effort on creating a script to
import the provided
[dump of raw data](https://s3.amazonaws.com/olapic-jobs-tests/test_hits.txt).

The script uses *argparse* to parse command line arguments.

Usage is explained in the [README](../README.md#loading-initial-data) file.

The script took ~13 seconds to import all of the 1171191 user actions:

```sh
INFO:Importer:Successfully imported 1171191 actions
INFO:Importer:Elapsed time: 0:13:43.699026
```

## API

The endpoint to obtain the conversion funnel was pretty straight forward, since
the data layer takes care of the heavy lifting.

I chose to return a 404 when the customer ID does not exist and also allow
date range filtering, not only for dates, but also for date times, up to
microseconds. The datetime interval is treated as left-closed and right-open,
i.e., [date_from, date_to).

Thanks to *webargs*, the endpoint allows for different date and datetime
formats, and it outputs nice error messages when dates or the range are invalid.

## Tests

For tests I used:

* [mock](https://github.com/testing-cabal/mock): allows to replace parts of the
system under test with mock objects and make assertions about how they have been
used.
* [factory-boy](https://factoryboy.readthedocs.io/): replaces static fixtures,
with easy-to-use factories for complex objects.
* [flask-testing](http://flask.pocoo.org/docs/latest/testing/): helpers to
create tests within a Flask application context.

The approach for creating test cases was that each component should be tested in
isolation, under the premise that its dependencies have already been thoroughly
tested.

For time constraints, I only added the minimal amount of tests, to cover all the
code, but some more test cases should be added, like integration tests.

This would aid, e.g., in finding cases where the API of an object changes and
the users of that object were not updated, which is a common pitfall when using
mock (which can not always be avoided with speccing).

To run the tests I use [nose](http://nose.readthedocs.io/en/latest/) with
[coverage](https://coverage.readthedocs.io) (for coverage reports), both
configured in the [`setup.cfg`](../setup.cfg) file.

## Application Manager

The application counts with a `manage.py` module, that I made using
[Flask-Script](https://flask-script.readthedocs.io/en/latest/), a great tool for
handling scripts, which, with just a few lines, enables two very useful
commands:

* runserver: runs the Flask development server (with debugger and auto-reload
capabilities)
* shell: start a Python shell in the context of the Flask application (with some
handy defaults already imported).

Other scripts could easily be turned into commands for this manager. A common
use case would be adding scripts to initialize the DB and run migrations (using
tools like *alembic* and *Flask-Migrate*).
