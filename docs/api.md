# API documentation

**Version**: 1.0.0

## Paths

### Customers

---
`GET /customers/<customer_id>/conversion_funnel`

Computes a conversion funnel for a customer.

Each funnel step is defined by the following events:

- Step 1
    - Widget render
- Step 2
    - Widget Move, Photo Upload, Photo Click
- Step 3
    - Checkout

**Parameters**

- `customer_id` (required): a customer ID.
- `date_from` (optional): a date or datetime.
- `date_to` (optional): a date or datetime.

NOTE: The date/datetime interval is computed as `[date_from, date_to)`.

**Response**

```
STATUS 200
{
    "data": {
            "customer": <customer_id>,
            "conversion_funnel":
            {
                "step_1": <number of user who reached step 1>,
                "step_2": <number of user who reached step 2>,
                "step_3": <number of user who reached step 3>
            }
        }
}
```