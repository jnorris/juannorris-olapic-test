# Project Architecture

Brief description about the project architecture.

## Technology Stack

* [PostgreSQL](www.postgresql.org.es/)
* Python
    * [Flask-SQLAlchemy](flask-sqlalchemy.pocoo.org): SQAlchemy ORM wrapper.
    * [Flask](flask.pocoo.org): Web Framework.
    * [Flask-RESTful](http://flask-restful.readthedocs.io/): RESTful API.
    * [webargs](https://webargs.readthedocs.io/): request params parser.
    * Tests: unittest, mock, factory-boy, flask-testing

## High Level Diagram

[Diagram](https://drive.google.com/file/d/0B0j_qAdViDhfNjRzUDN3Y00wa2s/view?usp=sharing)
created with *Draw.io*, which contemplates the whole application, rather than
just what is currently implemented.

## Back-end

There are two main components in the python application: core and web.

### Core

Data layer that handles database connection and queries using SQLAlchemy
toolkit and ORM.

Models are mapped auto-magically with the help of the Flask-SQLAlchemy
extension.

The core business logic is implemented here, and exposed in a way that the
consumers need to know only the interface.

### Web

A Flask web application, factored into a set of
[blueprints](http://flask.pocoo.org/docs/0.12/blueprints/).

*api* is the main blueprint, implemented with Flask-RESTful, where all resources
are located.

## Project Structure

```
/
  |- docs/
  |- olapic_app/
  |  |- core/
  |  |  |- factories/
  |  |  |- models.py
  |  |  |- <entity.py>
  |  |- scripts/
  |  |- web/
  |  |  |- api/
  |  |  |  |- representations/
  |  |  |  |- <resource.py>
  |- tests/
  |  |- core/
  |  |- web/
  |- .gitignore
  |- .pylintrc
  |- manage.py
  |- README.md
  |- requirements.txt
  |- requirements_dev.txt
  |- setup.cfg
```

* `docs/` - project documentation.
* `olapic_app/` - App root.
    * `core/` - data layer root.
      * `factories/` - test factories.
      * `models.py` - exposes all existing models.
      * `<entity.py>` - exposes models and functions related to an entity or
      entities.
    * `web/` - web application root.
        * `api/` - REST API resources and routing.
            * `representations/` - modules defining resource representations.
            * `<resource.py>` - exposes classes related to a specific resource
            or resources.
* `.pylintrc` - configuration file for PyLint.
* `manage.py` - application manager, used to run the dev server and other
scripts.
* `README.md` - quick start, environment setup, scripts usage.
* `requirements.txt` - python application dependencies.
* `requirements_dev.txt` - python development dependencies.
* `setup.cfg` - configuration file for python package (nose, pylint, coverage).

NOTE: The `tests` package, contains all tests and mimics the entire structure of
`olapic_app` package.
