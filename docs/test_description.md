# Olapic Data Engineering Project

## Conversion Funnel Metrics

### Objective

Build a system that computes and makes it available the results of a conversion funnel.

What is a conversion funnel?

A conversion funnel describes the sequence of steps in a user's journey to reach the checkout page. The shape of a funnel represents the gradual decline in the number of users who reach each step. Not all site visitors continue through to making a purchase.

In the case of Olapic, the conversion funnel is defined by the following three steps:

1. Widget View
2. Widget Interaction
3. Checkout

In order to "complete" the funnel a user must: view a widget then interact with it and, finally, purchase the product.

The goal here is to build a system that computes the conversion funnel and that enables us to retrieve the number of users that reached each of the funnel's steps

### Specific Case

In this test, you will be provided with a [dump of raw data](https://s3.amazonaws.com/olapic-jobs-tests/test_hits.txt). This dump file contains events that correspond to every single action performed by a user. The structure of the data file is the following:

customer_id | timestamp | user_id | event_id | device_type
--- | --- | --- | --- | ---
Id of the olapic customer | Event timestamp | id of the user | event (see event list below) | device

Event list

event_id | event description
:--- | :---
1 | Checkout
11 | Widget render
13 | Widget Move
14 | Photo Upload
15 | Photo Click


Each funnel step is defined by the following events:

- Step 1
    - Widget render
- Step 2
    - Widget Move, Photo Upload, Photo Click (step 2 is considered completed when the user performs at least one of any of these events)
- Step 3
    - Checkout

We expect you to implement an API endpoint so we can hit it with a customer_id as a parameter, and that will return a response containing the number of users who reached each step of the funnel. The endpoint should also have the possibility to filter the results by date range.

The format for the request and response should be as follows:

**Request**

```
GET /customers/{customer_id}/conversion_funnel{?date_from,date_to}
```


**Response**

```
STATUS 200
{
    "data": {
            "customer": [customer_id],
            "conversion_funnel":
            {
                "step_1": [number of user who reached step 1],
                "step_2": [number of user who reached step 2],
                "step_3": [number of user who reached step 3]
            }
        }
}
```

### Conditions

- To develop this project you can choose either PHP or Python.
- If necessary, you can use any web framework of your choice. In the case of PHP we recommend Silex and Flask for python.
- Design and document the architecture consciously. Flexibility, scalability, and usability should be honored by the design of your choice.
- You can use ANY  data store solution.
- The project should be correctly versioned using GIT. The GIT repository should be accessible to us privately. BitBucket is a good option.
- You don't need to serve the project to the internet but it should be testable locally. Make sure to include the proper documentation on how to run your project.
- Unit Tests are a big plus!
- All enhancements or added value you implement on top of the original idea will be highly appreciated (once more, remember to document them!)
- Have Fun!