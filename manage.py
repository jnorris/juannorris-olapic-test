"""
Script to initialize the Flask App.
"""

from olapic_app import core
from olapic_app.web import create_app

import config
from flask_script import Manager


def run():
    """
    Create and run the application.
    """
    app = create_app(config)

    # Initialize the script manager
    manager = Manager(app)

    # Include defaults in the shell
    @manager.shell
    def make_shell_context():  # pylint: disable=unused-variable
        """
        Make a shell context, to easily access the application, the db, and the
        models.
        """
        return dict(app=app, db=core.db, models=core)

    # Run the application
    manager.run()


if __name__ == '__main__':
    run()
