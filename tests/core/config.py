"""
Tests configuration module.
"""

import os


# Test DB URI, defaults to in memory SQLite
SQLALCHEMY_DATABASE_URI = os.getenv('OLAPIC_TEST_DATABASE_URI', None)

if not SQLALCHEMY_DATABASE_URI:
    raise ValueError('Missing DB URI env var: \'OLAPIC_TEST_DATABASE_URI\'')
