"""
Test cases for `olapic_app.core.user_hits`.
"""

import unittest
from collections import OrderedDict
from datetime import datetime, timedelta

import mock

from olapic_app.core import db, config, user_hits
from olapic_app.core.factories import user_hits_factories
from tests.core.utils import DBTestCase


def add_user_hits_per_event(customer_id, user_id, first_timestamp, seconds):
    """
    Add a user_hit for each event for the given `customer_id` and `user_id`,
    each with a sequential timestamp, separated by `seconds` (in either
    descending or ascending order).

    :param customer_id: a customer ID.
    :param user_id: a user ID.
    :param first_timestamp: the timestamp for the first user_hit.
    :param seconds: the seconds between each event.
    """
    dt_obj = first_timestamp

    # Add an entry for each event
    for event_id in config.INTERESTING_EVENTS:
        user_hits_factories.UserHitFactory(
            customer_id=customer_id,
            user_id=user_id,
            event_id=event_id,
            timestamp=dt_obj
        )

        dt_obj += timedelta(seconds=seconds)


class UserHitTestCase(unittest.TestCase):
    """
    Test for `user_hits.UserHit`.
    """

    def setUp(self):
        self.now = datetime.utcnow()

        self.test_user_hit = user_hits.UserHit(
            customer_id=100000,
            timestamp=self.now,
            user_id=1000,
            event_id=config.EVENT_CHECKOUT,
            device_type=10
        )

    def test_create_user_hit_instance(self):
        """
        Test creation of `user_hits.UserHit` instance.
        """
        self.assertIsInstance(self.test_user_hit, user_hits.UserHit)
        self.assertIsInstance(self.test_user_hit, db.Model)
        self.assertEqual(self.test_user_hit.customer_id, 100000)
        self.assertEqual(
            self.test_user_hit.timestamp,
            self.now.replace(tzinfo=None)
        )
        self.assertEqual(self.test_user_hit.user_id, 1000)
        self.assertEqual(self.test_user_hit.event_id, config.EVENT_CHECKOUT)
        self.assertEqual(self.test_user_hit.device_type, 10)
        self.assertEqual(
            self.test_user_hit.event_name,
            config.EVENT_CHECKOUT_NAME
        )

    def test_user_hit_repr(self):
        """
        Test `user_hits.UserHit.__repr__()`.
        """
        self.assertEqual(
            self.test_user_hit.__repr__(),
            '<UserHit: 1000, {}>'.format(config.EVENT_CHECKOUT_NAME)
        )


class UserHitDBTestCase(DBTestCase):
    """
    Tests the `user_hits.UserHit` Model with the database.
    """

    def setUp(self):
        self.test_user_hit = user_hits_factories.UserHitFactory(
            customer_id=100000,
            user_id=1000,
            event_id=config.EVENT_CHECKOUT,
            device_type=10
        )

    def test_insert_user_hit(self):
        """
        Test insertion of `user_hits.UserHit` into database.
        """
        user_hit = user_hits.UserHit.query.get(self.test_user_hit.id)
        self.assertIsInstance(user_hit, user_hits.UserHit)

    def test_delete_user_hit(self):
        """
        Test deletion of `user_hits.UserHit` from database.
        """
        user_hit = user_hits.UserHit.query.get(self.test_user_hit.id)

        self.session.delete(user_hit)
        self.session.commit()

        self.assertEqual(user_hits.UserHit.query.all(), [])

    def test_update_user_hit(self):
        """
        Test update of `user_hits.UserHit` from database.
        """
        self.test_user_hit.event_id = config.EVENT_DEFAULT

        self.session.add(self.test_user_hit)
        self.session.commit()

        new_user_hit = user_hits.UserHit.query.get(self.test_user_hit.id)

        self.assertEqual(new_user_hit.event_id, config.EVENT_DEFAULT)
        self.assertEqual(new_user_hit.event_name, config.EVENT_DEFAULT_NAME)


class CustomerExistsTestCase(DBTestCase):
    """
    Tests `user_hits.customer_exists`.
    """

    customer_id = 10

    def setUp(self):
        user_hits_factories.UserHitFactory(
            customer_id=self.customer_id
        )

    def test_customer_exists(self):
        """
        Ensure `True` is returned when the customer exists.
        """
        self.assertTrue(user_hits.customer_exists(self.customer_id))

    def test_customer_does_not_exist(self):
        """
        Ensure `False` is returned when the customer does not exist.
        """
        self.assertFalse(user_hits.customer_exists(123))


class ConversionFunnelByCustomerTestCase(DBTestCase):
    """
    Tests `user_hits.get_funnel_by_customer`.
    """

    customer_id = 10
    another_customer_id = 1000
    user_id = 1
    dt_now = datetime.utcnow()
    # Make sure all events are contained in the time window
    seconds_in_window = (
        float(config.TIME_WINDOW) / (len(config.INTERESTING_EVENTS) + 1)
    )

    def setUp(self):
        # Add entries for each event for the same user, in chronological order
        # and with a small time window between each other. This means 1 user
        # count for each step
        add_user_hits_per_event(
            self.customer_id, self.user_id, self.dt_now, self.seconds_in_window
        )

    @mock.patch.object(db.session, 'execute')
    def test_execute_without_rows(self, mock_execute):
        """
        Ensure a funnel is returned even if DB fails to return a row.
        """
        mock_execute.return_value.first.return_value = None

        self.assertDictEqual(
            user_hits.get_funnel_by_customer(
                self.another_customer_id
            ),
            OrderedDict(
                [(step, 0) for step in config.STEPS]
            )
        )

        mock_execute.assert_called_once()
        mock_execute.return_value.first.assert_called_once()

    def test_zero_count(self):
        """
        Ensure each step that has no count returns a 0.
        """
        # Add 1 user count for the first step, for a new customer
        user_hits_factories.UserHitFactory(
            customer_id=self.another_customer_id,
            user_id=self.user_id,
            event_id=config.EVENT_RENDER
        )

        self.assertDictEqual(
            user_hits.get_funnel_by_customer(
                self.another_customer_id
            ),
            {
                config.STEP_VIEW: 1,
                config.STEP_INTERACTION: 0,
                config.STEP_CHECKOUT: 0
            }
        )

    def test_only_given_customer_is_counted(self):
        """
        Ensure only entries for given customer get counted.
        """
        # Add 2 user count for the first step, for a new customer
        user_hits_factories.UserHitFactory(
            customer_id=self.another_customer_id,
            user_id=self.user_id,
            event_id=config.EVENT_RENDER
        )
        user_hits_factories.UserHitFactory(
            customer_id=self.another_customer_id,
            user_id=200,
            event_id=config.EVENT_RENDER
        )

        # Check the count is correct for each customer
        self.assertDictEqual(
            user_hits.get_funnel_by_customer(self.customer_id),
            {
                config.STEP_VIEW: 1,
                config.STEP_INTERACTION: 1,
                config.STEP_CHECKOUT: 1
            }
        )

        self.assertDictEqual(
            user_hits.get_funnel_by_customer(
                self.another_customer_id
            ),
            {
                config.STEP_VIEW: 2,
                config.STEP_INTERACTION: 0,
                config.STEP_CHECKOUT: 0
            }
        )

    def test_repeated_events_for_same_user(self):
        """
        Ensure the same user gets counted only once for each step.
        """
        # Add new, repeated entries, except for the timestamp, for the same
        # user
        for event_id in config.INTERESTING_EVENTS:
            user_hits_factories.UserHitFactory(
                customer_id=self.customer_id,
                user_id=self.user_id,
                event_id=event_id,
                timestamp=datetime.utcnow()
            )

        # Add some entries before the current date
        for event_id in config.INTERESTING_EVENTS:
            user_hits_factories.UserHitFactory(
                customer_id=self.customer_id,
                user_id=self.user_id,
                event_id=event_id,
                timestamp=self.dt_now - timedelta(days=3)
            )

        # Check the count hasn't changed
        self.assertDictEqual(
            user_hits.get_funnel_by_customer(self.customer_id),
            {
                config.STEP_VIEW: 1,
                config.STEP_INTERACTION: 1,
                config.STEP_CHECKOUT: 1
            }
        )

    def test_with_several_users(self):
        """
        Ensure the count is correct with several users and events.
        """
        # Add an entry for each event, for a new user. Adds 1 count per step
        add_user_hits_per_event(
            self.customer_id, 2, self.dt_now, self.seconds_in_window
        )

        # Add an entry for a new user for step 1. Adds 1 count to step 1
        user_hits_factories.UserHitFactory(
            customer_id=self.customer_id,
            user_id=3,
            event_id=config.EVENT_RENDER
        )

        # Add an entry for a new user for steps 1 and 2. Adds 1 count for step
        # 1 and 1 for step 2
        user_hits_factories.UserHitFactory(
            customer_id=self.customer_id,
            user_id=4,
            event_id=config.EVENT_RENDER,
            timestamp=self.dt_now
        )
        user_hits_factories.UserHitFactory(
            customer_id=self.customer_id,
            user_id=4,
            event_id=config.EVENT_MOVE
        )

        # Add entries for new users in step 2. Adds 3 counts for step 2
        user_hits_factories.UserHitFactory(
            customer_id=self.customer_id,
            user_id=5,
            event_id=config.EVENT_MOVE
        )
        user_hits_factories.UserHitFactory(
            customer_id=self.customer_id,
            user_id=6,
            event_id=config.EVENT_PHOTO_CLICK
        )
        user_hits_factories.UserHitFactory(
            customer_id=self.customer_id,
            user_id=7,
            event_id=config.EVENT_MOVE,
            timestamp=self.dt_now + timedelta(seconds=self.seconds_in_window)
        )

        # Finally, the count should be the following
        self.assertDictEqual(
            user_hits.get_funnel_by_customer(self.customer_id),
            {
                config.STEP_VIEW: 4,
                config.STEP_INTERACTION: 2,
                config.STEP_CHECKOUT: 2
            }
        )

    def test_events_not_in_sequence(self):
        """
        Ensure events not in sequence are not counted.
        """
        # Add entries for each event for new users. These only add a count for
        # step 1
        for event_id in config.INTERESTING_EVENTS:
            user_hits_factories.UserHitFactory(
                customer_id=self.customer_id,
                event_id=event_id
            )

        self.assertDictEqual(
            user_hits.get_funnel_by_customer(self.customer_id),
            {
                config.STEP_VIEW: 2,
                config.STEP_INTERACTION: 1,
                config.STEP_CHECKOUT: 1
            }
        )

    def test_events_not_in_chronological_order(self):
        """
        Ensure events are only counted if occurring in chronological order.
        """
        # Add entries in reverse chronological order
        add_user_hits_per_event(
            self.customer_id, 2, self.dt_now, -self.seconds_in_window
        )

        self.assertDictEqual(
            user_hits.get_funnel_by_customer(self.customer_id),
            {
                config.STEP_VIEW: 2,
                config.STEP_INTERACTION: 1,
                config.STEP_CHECKOUT: 1
            }
        )

    def test_events_not_inside_the_time_window(self):
        """
        Ensure events outside the time window are not counted.
        """
        # Add entries happening past the time window
        add_user_hits_per_event(
            self.customer_id, 2, self.dt_now, config.TIME_WINDOW + 100
        )

        self.assertDictEqual(
            user_hits.get_funnel_by_customer(self.customer_id),
            {
                config.STEP_VIEW: 2,
                config.STEP_INTERACTION: 1,
                config.STEP_CHECKOUT: 1
            }
        )


class ConversionFunnelWithDateRangeTestCase(DBTestCase):
    """
    Tests `user_hits.get_funnel_by_customer` with datetime ranges.
    """

    customer_id = 2001
    dt_now = datetime.utcnow()
    # Make sure all events are contained in the time window
    seconds_in_window = (
        config.TIME_WINDOW / (len(config.INTERESTING_EVENTS) + 1)
    )

    def setUp(self):
        # Add an entry for each event, for a new user. Adds 1 count per step
        add_user_hits_per_event(
            self.customer_id, 1, self.dt_now, self.seconds_in_window
        )

        # Add step1, step2 and step3 for another user
        user_hits_factories.UserHitFactory(
            customer_id=self.customer_id,
            user_id=2,
            event_id=config.EVENT_RENDER,
            timestamp=self.dt_now
        )
        user_hits_factories.UserHitFactory(
            customer_id=self.customer_id,
            user_id=2,
            event_id=config.EVENT_MOVE,
            timestamp=self.dt_now + timedelta(seconds=self.seconds_in_window)
        )
        # But checkout is past the time window
        user_hits_factories.UserHitFactory(
            customer_id=self.customer_id,
            user_id=2,
            event_id=config.EVENT_CHECKOUT,
            timestamp=(
                self.dt_now + timedelta(seconds=self.seconds_in_window) +
                timedelta(seconds=config.TIME_WINDOW + 1)
            )
        )

        # Add 5 hits between date times: [today - 4seconds, today]
        seconds_range = xrange(
            0, 5 * self.seconds_in_window, self.seconds_in_window
        )
        for seconds in seconds_range:
            user_hits_factories.UserHitFactory(
                customer_id=self.customer_id,
                event_id=config.EVENT_RENDER,
                timestamp=self.dt_now + timedelta(seconds=seconds)
            )

    @mock.patch.object(config, 'STEPS', new=[])
    def test_no_steps(self):
        """
        Ensure an empty dictionary is returned when there are no steps.
        """
        funnel = user_hits.get_funnel_by_customer(
            self.customer_id
        )

        self.assertDictEqual(funnel, {})

    def test_no_range(self):
        """
        Ensure all hits are returned when no datetime range is used.
        """
        funnel = user_hits.get_funnel_by_customer(
            self.customer_id
        )

        self.assertDictEqual(
            funnel,
            {
                config.STEP_VIEW: 7,
                config.STEP_INTERACTION: 2,
                config.STEP_CHECKOUT: 1
            }
        )

    def test_datetime_from(self):
        """
        Ensure only hits after given datetime are returned.
        """
        funnel = user_hits.get_funnel_by_customer(
            self.customer_id,
            date_from=self.dt_now + timedelta(
                seconds=2 * self.seconds_in_window
            )
        )

        self.assertDictEqual(
            funnel,
            {
                config.STEP_VIEW: 3,
                config.STEP_INTERACTION: 0,
                config.STEP_CHECKOUT: 0
            }
        )

    def test_datetime_to(self):
        """
        Ensure only hits before given datetime are returned.
        """
        funnel = user_hits.get_funnel_by_customer(
            self.customer_id,
            date_to=self.dt_now + timedelta(seconds=2 * self.seconds_in_window)
        )

        self.assertDictEqual(
            funnel,
            {
                config.STEP_VIEW: 4,
                config.STEP_INTERACTION: 2,
                config.STEP_CHECKOUT: 0
            }
        )

    def test_datetime_range(self):
        """
        Ensure only hits between the given datetime range are returned.
        """
        date_from = self.dt_now + timedelta(seconds=2 * self.seconds_in_window)
        date_to = self.dt_now + timedelta(seconds=4 * self.seconds_in_window)

        funnel = user_hits.get_funnel_by_customer(
            self.customer_id,
            date_from=date_from,
            date_to=date_to
        )

        self.assertDictEqual(
            funnel,
            {
                config.STEP_VIEW: 2,
                config.STEP_INTERACTION: 0,
                config.STEP_CHECKOUT: 0
            }
        )

    def test_date_range(self):
        """
        Ensure a range of dates is also valid.
        """
        funnel = user_hits.get_funnel_by_customer(
            self.customer_id,
            date_from=self.dt_now.date(),
            date_to=self.dt_now.date() + timedelta(days=1)
        )

        self.assertDictEqual(
            funnel,
            {
                config.STEP_VIEW: 7,
                config.STEP_INTERACTION: 2,
                config.STEP_CHECKOUT: 1
            }
        )


class GetEventsExpressionTestCase(DBTestCase):
    """
    Tests for `user_hits._get_events_expression`.
    """

    dict_get_patcher = mock.patch.object(config.STEP_TO_EVENTS, 'get')

    def setUp(self):
        self.mock_dict_get = self.dict_get_patcher.start()

    def tearDown(self):
        mock.patch.stopall()

    def test_invalid_step(self):
        """
        Ensure a `ValueError` is raised when the step is not valid.
        """
        self.mock_dict_get.return_value = None

        self.assertRaisesRegexp(
            ValueError,
            'Invalid step',
            user_hits.get_events_expression,
            user_hits.UserHit,
            'no_step'
        )

        self.mock_dict_get.assert_called_once_with(
            'no_step', None
        )

    def test_binary_expression_for_one_event(self):
        """
        Ensure a comparison `BinaryExpression` is returned for 1 event steps.
        """
        self.mock_dict_get.return_value = [1]

        binary_expression = user_hits.get_events_expression(
            user_hits.UserHit,
            'step1'
        )

        self.assertEqual(
            str(binary_expression),
            'user_hit.event_id = :event_id_1'
        )

        self.mock_dict_get.assert_called_once_with('step1', None)

    def test_binary_expression_for_many_event(self):
        """
        Ensure an IN `BinaryExpression` is returned for many events steps.
        """
        self.mock_dict_get.return_value = [1, 2]

        binary_expression = user_hits.get_events_expression(
            user_hits.UserHit,
            'step2'
        )

        self.assertEqual(
            str(binary_expression),
            'user_hit.event_id IN (:event_id_1, :event_id_2)'
        )

        self.mock_dict_get.assert_called_once_with('step2', None)
