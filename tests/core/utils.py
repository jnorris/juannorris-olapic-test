"""
Utils for database related tests.
"""

import unittest

from olapic_app.core.base import db
from tests.core import config

# pylint: disable=invalid-name
# Create an engine and session just once for testing purposes
engine = db.create_engine(config.SQLALCHEMY_DATABASE_URI)
db.session = db.scoped_session(db.sessionmaker(autoflush=False, bind=engine))


def get_db_session():
    """
    Return a scoped session for the test suite.
    """
    return db.session


class DBTestCase(unittest.TestCase):
    """
    Test case that provides an `SQLAlchemy` session.

    Create all tables into the database on a pre setup step, and drop them on a
    post teardown step.
    """

    engine = engine
    session = get_db_session()
    connection = None

    def __call__(self, *args, **kwds):
        """
        Do the required setup and teardown.

        Doing it here removes the need to call `super.setUp` in subclasses.
        """
        try:
            self._pre_setup()
            super(DBTestCase, self).__call__(*args, **kwds)
        finally:
            self._post_teardown()

    def _pre_setup(self):
        """
        Open DB connection and bind its session to this object.
        """
        self.connection = self.engine.connect()

        db.Model.metadata.create_all(self.engine)

    def _post_teardown(self):
        """
        Close session, connection and drop all tables.
        """
        self.session.close()
        self.connection.close()

        db.Model.metadata.drop_all(self.engine)
