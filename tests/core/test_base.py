"""
Test cases for `olapic_app.core.base`.
"""

import datetime
import unittest

import pytz
from sqlalchemy.dialects import postgresql

from olapic_app.core import base
from olapic_app.core.base import db
from tests.core.utils import DBTestCase


class DummyModel(db.Model):
    """
    Dummy Class model for testing purposes.

    Aids on checking that `base.BaseModel` works well as a base model from
    where other models can inherit.
    """

    __tablename__ = 'dummy'

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(16))
    type = db.Column(db.String(16))


class HelperTestCase(DBTestCase):
    """
    Base helper TestCase that initializes common data.
    """

    day = datetime.datetime.now(pytz.utc)
    name = 'test_name'

    def setUp(self):
        # Insert a new dummy
        self.dummy = DummyModel(
            id=2,
            name=self.name,
            created_on=self.day,
            last_modified=self.day
        )

        self.session.add(self.dummy)
        self.session.commit()


class BaseModelTestCase(unittest.TestCase):
    """
    Tests for `base.BaseModel.to_json` and `base.BaseModel.__repr__`.
    """

    day = datetime.datetime.utcnow()
    name = 'test_name'

    def setUp(self):
        super(BaseModelTestCase, self).setUp()

        self.dummy = DummyModel(
            id=3,
            name=self.name,
            created_on=self.day,
            last_modified=self.day
        )

    def test_base_to_json(self):
        """
        Ensure the correct JSON is returned.
        """
        json_data = self.dummy.to_json()

        self.assertEqual(json_data.get('id'), 3)
        self.assertEqual(json_data.get('name'), self.name)
        self.assertEqual(json_data.get('created_on'), self.day.isoformat())
        self.assertEqual(json_data.get('last_modified'), self.day.isoformat())

    def test_base_repr(self):
        """
        Ensure the right representation is displayed.
        """
        self.assertEqual(self.dummy.__repr__(), unicode(self.dummy.__dict__))


class UTCDateTimeProcessBindParamTestCase(unittest.TestCase):
    """
    Test `UTCDateTime.process_result_value`.
    """

    def setUp(self):
        self.aware_datetime = datetime.datetime.now().replace(tzinfo=pytz.utc)
        self.naive_datetime = self.aware_datetime.replace(tzinfo=None)
        self.type_decorator = base.UTCDateTime()

    def test_process_bind_param(self):
        """
        Ensure `UTCDateTime.process_bind_param` returns naive datetime.
        """
        result = self.type_decorator.process_bind_param(
            self.aware_datetime,
            dialect=postgresql
        )
        self.assertIsNone(result.tzinfo)
        self.assertEqual(
            result,
            self.aware_datetime.astimezone(pytz.utc).replace(tzinfo=None)
        )

    def test_process_bind_param_no_input(self):
        """
        Ensure `UTCDateTime.process_bind_param` works for None.
        """
        result = self.type_decorator.process_bind_param(
            None,
            dialect=postgresql
        )

        self.assertIsNone(result)

    def test_process_bind_param_naive_datetime(self):
        """
        Ensure `UTCDateTime.process_bind_param` works for naive datetime.
        """
        result = self.type_decorator.process_bind_param(
            self.naive_datetime,
            dialect=postgresql
        )

        self.assertEqual(result, self.naive_datetime)

    def test_process_result_value(self):
        """
        Ensure `UTCDateTime.process_result_value` returns aware UTC datetime.
        """
        result = self.type_decorator.process_result_value(
            self.naive_datetime,
            dialect=postgresql
        )

        self.assertEqual(result.tzinfo, pytz.utc)
        self.assertEqual(result, self.aware_datetime)

    def test_process_result_value_invalid_input(self):
        """
        Ensure `UTCDateTime.process_result_value` works for None.
        """
        result = self.type_decorator.process_result_value(
            None,
            dialect=postgresql
        )

        self.assertIsNone(result)

    def test_process_bind_param_aware_datetime(self):
        """
        Ensure `UTCDateTime.process_result_value` works for aware datetime.
        """
        result = self.type_decorator.process_result_value(
            self.aware_datetime,
            dialect=postgresql
        )

        self.assertEqual(result, self.aware_datetime)


class DatabaseTestCase(HelperTestCase):
    """
    Test initial database configuration.
    """

    def test_db_insert(self):
        """
        Test inserting into the database.
        """
        dummy_obj = DummyModel.query.get(2)

        self.assertIsInstance(dummy_obj, DummyModel)
        self.assertEqual(dummy_obj.created_on, self.day)

    def test_db_delete(self):
        """
        Test deleting a element from the database.
        """
        dummy_obj = DummyModel.query.get(2)

        self.session.delete(dummy_obj)
        self.session.commit()

        self.assertEqual(DummyModel.query.all(), [])

    def test_db_update(self):
        """
        Test updating a element from the database.
        """
        new_day = datetime.datetime.now(pytz.utc)

        dummy_obj = DummyModel.query.get(2)

        dummy_obj.created_on = new_day

        self.session.add(dummy_obj)
        self.session.commit()

        dummy_obj = DummyModel.query.get(2)

        self.assertEqual(dummy_obj.created_on, new_day)
