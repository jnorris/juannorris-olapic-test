"""
Test cases for `olapic_app.core.config`.
"""

import unittest

from mock import patch

from olapic_app.core import config


class TypesTestCase(unittest.TestCase):
    """
    Test `config.EVENT_TYPES` constant.
    """

    def test_event_constants(self):
        """
        Ensure event constants are well defined.
        """
        self.assertEqual(config.EVENT_DEFAULT, -1)
        self.assertEqual(config.EVENT_DEFAULT_NAME, 'Other')

        self.assertEqual(config.EVENT_CHECKOUT, 1)
        self.assertEqual(config.EVENT_CHECKOUT_NAME, 'Checkout')

        self.assertEqual(config.EVENT_RENDER, 11)
        self.assertEqual(config.EVENT_RENDER_NAME, 'Widget Render')

        self.assertEqual(config.EVENT_MOVE, 13)
        self.assertEqual(config.EVENT_MOVE_NAME, 'Widget Move')

        self.assertEqual(config.EVENT_PHOTO_UPLOAD, 14)
        self.assertEqual(config.EVENT_PHOTO_UPLOAD_NAME, 'Photo Upload')

        self.assertEqual(config.EVENT_PHOTO_CLICK, 15)
        self.assertEqual(config.EVENT_PHOTO_CLICK_NAME, 'Photo Click')

    def test_event_types(self):
        """
        Test `config.EVENT_TYPES`.
        """
        self.assertEqual(
            config.EVENT_TYPES.get(config.EVENT_DEFAULT),
            config.EVENT_DEFAULT_NAME
        )
        self.assertEqual(
            config.EVENT_TYPES.get(config.EVENT_CHECKOUT),
            config.EVENT_CHECKOUT_NAME
        )
        self.assertEqual(
            config.EVENT_TYPES.get(config.EVENT_RENDER),
            config.EVENT_RENDER_NAME
        )
        self.assertEqual(
            config.EVENT_TYPES.get(config.EVENT_MOVE),
            config.EVENT_MOVE_NAME
        )
        self.assertEqual(
            config.EVENT_TYPES.get(config.EVENT_PHOTO_UPLOAD),
            config.EVENT_PHOTO_UPLOAD_NAME
        )
        self.assertEqual(
            config.EVENT_TYPES.get(config.EVENT_PHOTO_CLICK),
            config.EVENT_PHOTO_CLICK_NAME
        )


class GetTypeNameTestCase(unittest.TestCase):
    """
    Test `config.get_event_type_display` function.
    """

    event_type = 'some event'

    def test_get_event_type_display_unknow_type(self):
        """
        Ensure it returns `config.EVENT_DEFAULT_NAME` for unknown type.
        """
        result = config.get_event_type_display(self.event_type)
        self.assertEqual(result, config.EVENT_DEFAULT_NAME)

    @patch.object(config, 'EVENT_TYPES')
    def test_get_event_type_display_known_type(self, mock_event_types):
        """
        Ensure every constant returns the right display name.
        """
        mock_event_types.get.return_value = 'display name'

        result = config.get_event_type_display(self.event_type)

        self.assertEqual(result, 'display name')

        mock_event_types.get.assert_called_once_with(
            self.event_type,
            config.EVENT_DEFAULT_NAME
        )


class StepsTestCase(unittest.TestCase):
    """
    Test `config.STEPS` and `config.STEP_TO_EVENTS` config.
    """

    def test_step_constants(self):
        """
        Ensure step constants are well defined.
        """
        self.assertEqual(config.STEP_VIEW, 'step_1')
        self.assertEqual(config.STEP_INTERACTION, 'step_2')
        self.assertEqual(config.STEP_CHECKOUT, 'step_3')

        self.assertEqual(config.EVENT_MOVE, 13)
        self.assertEqual(config.EVENT_MOVE_NAME, 'Widget Move')

        self.assertEqual(config.EVENT_PHOTO_UPLOAD, 14)
        self.assertEqual(config.EVENT_PHOTO_UPLOAD_NAME, 'Photo Upload')

        self.assertEqual(config.EVENT_PHOTO_CLICK, 15)
        self.assertEqual(config.EVENT_PHOTO_CLICK_NAME, 'Photo Click')

    def test_steps(self):
        """
        Ensure `config.STEPS` has the right values.
        """
        self.assertListEqual(
            config.STEPS,
            [
                config.STEP_VIEW,
                config.STEP_INTERACTION,
                config.STEP_CHECKOUT
            ]
        )

    def test_step_to_events(self):
        """
        Ensure `config.STEP_TO_EVENTS` has the right values.
        """
        self.assertListEqual(
            config.STEP_TO_EVENTS.get(config.STEP_VIEW),
            [config.EVENT_RENDER]
        )

        self.assertListEqual(
            config.STEP_TO_EVENTS.get(config.STEP_INTERACTION),
            [
                config.EVENT_MOVE,
                config.EVENT_PHOTO_UPLOAD,
                config.EVENT_PHOTO_CLICK
            ]
        )

        self.assertListEqual(
            config.STEP_TO_EVENTS.get(config.STEP_CHECKOUT),
            [config.EVENT_CHECKOUT]
        )

    def test_interesting_events(self):
        """
        Ensure `config.INTERESTING_EVENTS` has the right values.
        """
        self.assertListEqual(
            config.INTERESTING_EVENTS,
            [
                config.EVENT_RENDER,
                config.EVENT_MOVE,
                config.EVENT_PHOTO_UPLOAD,
                config.EVENT_PHOTO_CLICK,
                config.EVENT_CHECKOUT
            ]
        )


class TimeWindowTestCase(unittest.TestCase):
    """
    Test `config.TIME_WINDOW` constant.
    """

    def test_step_constants(self):
        """
        Ensure `config.TIME_WINDOW` is defined as 30 minutes, in seconds.
        """
        self.assertEqual(config.TIME_WINDOW, 1800)
