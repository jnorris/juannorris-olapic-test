"""
Test cases for `olapic_app.core.utils`.
"""

from datetime import datetime
import unittest

from olapic_app.core import utils


class ToNaiveTestCase(unittest.TestCase):
    """
    Tests for `utils.datetime_to_naive`.
    """

    def setUp(self):
        super(ToNaiveTestCase, self).setUp()

        self.aware_dt = datetime.utcnow()

    def test_naive_from_aware(self):
        """
        Ensure a naive datetime is returned from an aware datetime.
        """
        naive_dt = utils.datetime_to_naive(self.aware_dt)

        self.assertIsNone(naive_dt.tzinfo)

    def test_naive_from_naive(self):
        """
        Ensure the same datetime is returned for naive datetime.
        """
        naive_dt = datetime.now()
        naive_dt_result = utils.datetime_to_naive(naive_dt)

        self.assertIsNone(naive_dt_result.tzinfo)


class DateToDatetimeTestCase(unittest.TestCase):
    """
    Tests for `utils.date_to_datetime`.
    """

    def test_non_date(self):
        """
        Ensure non date objects are returned unchanged.
        """
        obj = {}
        self.assertEqual(obj, utils.date_to_datetime(obj))

        obj = datetime.utcnow()
        self.assertEqual(obj, utils.date_to_datetime(obj))

    def test_datetime_from_date(self):
        """
        Ensure the correct datetime is returned from a date.
        """
        date_obj = datetime.utcnow().date()
        dt_obj = utils.date_to_datetime(date_obj)

        self.assertIsInstance(dt_obj, datetime)
        self.assertEqual(dt_obj.year, date_obj.year)
        self.assertEqual(dt_obj.month, date_obj.month)
        self.assertEqual(dt_obj.day, date_obj.day)
        self.assertEqual(dt_obj.hour, 0)
        self.assertEqual(dt_obj.minute, 0)
        self.assertEqual(dt_obj.second, 0)
        self.assertEqual(dt_obj.microsecond, 0)
