"""
Test utilities module.
"""

import flask
import flask_testing

from tests.core.utils import DBTestCase


def default_make_app(blueprint=None):
    """
    Create Flask application.

    If a blueprint is provided, initialize it.

    :param blueprint: Optional Flask blueprint instance.
    :returns: Flask application instance.
    """
    app = flask.Flask(__name__)

    app.config.update({
        'SECRET_KEY': 'VERY_SECRET'
    })

    if blueprint:
        blueprint.init_app(app)

    return app


class BaseWebTestCase(flask_testing.TestCase):
    """
    Base test class to reuse app creation and run tests within a Flask
    application context.

    :ivar make_app: sub classes can override this variable to define a custom
    function to create the application.
    :ivar blueprint: a Flask blueprint that will be part of the Flask app, when
    present.
    """

    make_app = None
    blueprint = None

    def create_app(self):
        """
        Create a Flask app for testing purposes.

        :return: a Flask application instance.
        """
        _make_app = self.make_app or default_make_app

        return _make_app(self.blueprint)


class BaseWebDBTestCase(BaseWebTestCase, DBTestCase):
    """
    Base test class for DB and Flask app testing.
    """

    def _pre_setup(self):
        super(BaseWebDBTestCase, self)._pre_setup()
        DBTestCase._pre_setup(self)

    def _post_teardown(self):
        super(BaseWebDBTestCase, self)._post_teardown()
        DBTestCase._post_teardown(self)
