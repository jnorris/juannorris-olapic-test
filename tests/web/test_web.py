"""
Test cases for `olapic_app.web`.
"""

import unittest

import mock

from olapic_app import web


class BlueprintsTestCase(unittest.TestCase):
    """
    Tests for `web.BLUEPRINTS`.
    """

    def test_blueprints_list(self):
        """
        Ensure all blueprints have been added.
        """
        self.assertListEqual(web.BLUEPRINTS, [
            web.api
        ])


class CreateAppTestCase(unittest.TestCase):
    """
    Tests for `web.create_app`.
    """

    config_module = mock.Mock()
    # Patch Flask object
    flask_patcher = mock.patch.object(web, 'Flask', autospec=True)
    # Patch db.init_app
    db_init_patcher = mock.patch.object(web.db, 'init_app', autospec=True)

    def setUp(self):
        # Mock Flask object
        self.mock_flask = self.flask_patcher.start()
        # Mock db object
        self.mock_db_init = self.db_init_patcher.start()

        # Ensure the Flask instance has a config object
        self.instance = self.mock_flask.return_value
        self.instance.configure_mock(**{'config': mock.Mock()})

    def tearDown(self):
        mock.patch.stopall()

    def test_creation_app(self):
        """
        Ensure the creation of the Flask application.
        """
        app = web.create_app(self.config_module)

        self.mock_flask.assert_called_once_with(
            'olapic_app.web'
        )
        self.mock_db_init.assert_called_once_with(app)
        self.instance.config.from_object.assert_called_once_with(
            self.config_module
        )

    @mock.patch.object(web, 'BLUEPRINTS', new_calleable=mock.PropertyMock)
    def test_create_app_with_blueprints(self, mock_blueprints):
        """
        Ensure blueprints are required to have an init_app method.
        """
        # Mock a blueprint with init_app method
        mock_bp = mock.Mock(spec=['__name__', '__getitem__', 'init_app'])
        mock_blueprints.__iter__.return_value = [mock_bp]

        app = web.create_app(self.config_module)

        mock_bp.init_app.assert_called_once_with(app)

    @mock.patch.object(web, 'BLUEPRINTS', new_calleable=mock.PropertyMock)
    def test_create_app_with_no_init_bp(self, mock_blueprints):
        """
        Ensure blueprints are required to have an init_app method.
        """
        # Mock a blueprint without init_app method
        mock_bp = mock.Mock(spec=['__name__', '__getitem__'])
        mock_blueprints.__iter__.return_value = [mock_bp]

        error_msg = '%r has no init_app' % (mock_bp.__name__,)

        with self.assertRaisesRegexp(AttributeError, error_msg):
            web.create_app(self.config_module)
