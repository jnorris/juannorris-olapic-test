"""
Tests cases for `olapic_app.web.api.representations.custom_json_encoder`.
"""

import unittest
from datetime import datetime

import mock

from olapic_app.web.api.representations import custom_json_encoder


class CustomJSONEncoderTestCase(unittest.TestCase):
    """
    Test `json.CustomJSONEncoder`
    """

    json_encoder = custom_json_encoder.CustomJSONEncoder()

    @mock.patch.object(custom_json_encoder, 'to_timestamp')
    def test_datetime_encoding(self, mock_to_timestamp):
        """
        Ensure a timestamp is returned for a datetime.
        """
        mock_to_timestamp.return_value = 'timestamp'

        dt_obj = datetime.utcnow()

        result = self.json_encoder.default(dt_obj)

        self.assertEqual(result, 'timestamp')

        mock_to_timestamp.assert_called_once_with(dt_obj)

    @mock.patch('flask.json.JSONEncoder.default')
    def test_default_encoder(self, mock_json_default):
        """
        Ensure Flask's JSONEncoder is used for default cases.
        """
        mock_json_default.return_value = 'default_encoder'

        result = self.json_encoder.default({'a': '2'})

        self.assertEqual(result, 'default_encoder')

        mock_json_default.assert_called_once_with(
            self.json_encoder,
            {'a': '2'}
        )
