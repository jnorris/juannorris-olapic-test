"""
Tests cases for `olapic_app.web.api.user_hits`.
"""

import unittest
from datetime import datetime

import mock
from marshmallow import fields
from werkzeug.exceptions import NotFound

from olapic_app.web.api import base, user_hits
from tests.web import utils as test_utils


class UserHitsRoutingTestCase(test_utils.BaseWebTestCase):
    """
    Tests for `user_hits` routes.
    """

    blueprint = base

    def test_routes_for_customer_conversion_funnel(self):
        """
        Validate `user_hits.CustomerConversionFunnel` routes.
        """
        self.assertEqual(
            base.restful_api.url_for(
                user_hits.CustomerConversionFunnel,
                customer_id=101
            ),
            '/api/v1/customers/101/conversion_funnel'
        )


class CustomerExistsRouting(unittest.TestCase):
    """
    Tests for `user_hits.customer_exists`.
    """

    customer_exists_patcher = mock.patch(
        'olapic_app.core.user_hits.customer_exists'
    )

    customer_id = 101

    def setUp(self):
        self.mock_customer_exists = self.customer_exists_patcher.start()
        self.addCleanup(self.customer_exists_patcher.stop)

    def test_customer_does_not_exist(self):
        """
        Ensure `NotFound` is raised when the customer does not exist.
        """
        self.mock_customer_exists.return_value = False

        with self.assertRaises(NotFound) as ctx:
            user_hits.customer_exists(2121)

        self.assertEqual(ctx.exception.description, 'Customer does not exist')
        self.mock_customer_exists.assert_called_once_with(2121)

    def test_customer_exists(self):
        """
        Ensure `None` is returned when the customer exists.
        """
        self.mock_customer_exists.return_value = True

        self.assertIsNone(
            user_hits.customer_exists(self.customer_id)
        )

        self.mock_customer_exists.assert_called_once_with(self.customer_id)


class ValidateDatesTestCase(test_utils.BaseWebTestCase):
    """
    Tests for `user_hits.validate_dates`.
    """

    blueprint = base

    def test_validate_no_dates(self):
        """
        Ensure validator passes when both dates are not present.
        """
        self.assertTrue(user_hits.validate_dates({}))
        self.assertTrue(
            user_hits.validate_dates(
                {'date_from': datetime.utcnow()}
            )
        )
        self.assertTrue(
            user_hits.validate_dates(
                {'date_to': datetime.utcnow()}
            )
        )

    def test_valid_range(self):
        """
        Ensure validator passes when dates are a valid range.
        """
        self.assertTrue(
            user_hits.validate_dates(
                {
                    'date_from': datetime(2017, 02, 03, 1, 1, 1, 9),
                    'date_to': datetime(2017, 02, 03, 1, 1, 1, 10)
                }
            )
        )

    def test_invalid_range(self):
        """
        Ensure validator raises `ValidationError` when range is invalid.
        """
        from webargs import ValidationError
        self.assertRaisesRegexp(
            ValidationError,
            '\'date_from\' must be before \'date_to\'',
            user_hits.validate_dates,
            {
                'date_from': datetime(2017, 02, 03, 1, 1, 1, 10),
                'date_to': datetime(2017, 02, 03, 1, 1, 1, 10)
            }
        )

        self.assertRaisesRegexp(
            ValidationError,
            '\'date_from\' must be before \'date_to\'',
            user_hits.validate_dates,
            {
                'date_from': datetime(2017, 02, 03, 1, 1, 1, 11),
                'date_to': datetime(2017, 02, 03, 1, 1, 1, 10)
            }
        )


class ConversionFunnelTestCase(test_utils.BaseWebTestCase):
    """
    Tests for `user_hits.CustomerConversionFunnel`.
    """

    blueprint = base
    customer_id = 101

    def test_date_args(self):
        """
        Ensure the date args have the right type.
        """
        ccf = user_hits.CustomerConversionFunnel()

        self.assertIsInstance(ccf.date_args['date_from'], fields.DateTime)
        self.assertIsInstance(ccf.date_args['date_to'], fields.DateTime)

    @mock.patch.object(user_hits, 'customer_exists')
    @mock.patch('olapic_app.core.user_hits.get_funnel_by_customer')
    def test_get(self, get_funnel_mock, customer_exists_mock):
        """
        Ensure `CustomerConversionFunnel.get` method returns the expected json.
        """
        get_funnel_mock.return_value = 'conversion_funnel_dict'

        url = base.restful_api.url_for(
            user_hits.CustomerConversionFunnel,
            customer_id=self.customer_id
        )

        response = self.client.get(url)

        get_funnel_mock.assert_called_once_with(
            self.customer_id,
            date_from=None,
            date_to=None
        )
        customer_exists_mock.assert_called_once_with(self.customer_id)

        self.assertStatus(response, 200)
        self.assertDictEqual(response.json, {
            'data': {
                'customer': self.customer_id,
                'conversion_funnel': 'conversion_funnel_dict'
            }
        })

    def test_get_with_invalid_dates(self):
        """
        Ensure the right HTTP error is returned for invalid dates.
        """
        url = base.restful_api.url_for(
            user_hits.CustomerConversionFunnel,
            customer_id=self.customer_id,
            date_from='not_a_date',
            date_to='not_a_date'
        )

        response = self.client.get(url)

        self.assertStatus(response, 422)

    def test_get_with_date_from_after_date_to(self):
        """
        Ensure the right HTTP error and message is returned for invalid dates.
        """
        url = base.restful_api.url_for(
            user_hits.CustomerConversionFunnel,
            customer_id=self.customer_id,
            date_from='2015-10-04 20:48:45.919',
            date_to='2015-10-03 20:48:45.919'
        )

        response = self.client.get(url)

        self.assertStatus(response, 422)
        self.assertDictEqual(response.json, {
            'messages': [
                '\'date_from\' must be before \'date_to\''
            ]
        })
