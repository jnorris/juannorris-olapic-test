"""
Tests cases for `olapic_app.web.api.base`.
"""

import unittest

import flask

from olapic_app.web.api import base


class ApiTestCase(unittest.TestCase):
    """
    Tests for `api`.
    """

    def test_api(self):
        """
        Ensure that `api.api` is an instance of `restful.Api`,
        and `api.blueprint` has the correct parameters.
        """
        self.assertIsInstance(base.restful_api, base.restful.Api)
        self.assertEqual(base.blueprint.name, 'olapic_app.api')
        self.assertEqual(base.blueprint.import_name, 'olapic_app.web.api.base')
        self.assertEqual(base.blueprint.url_prefix, '/api/v1')

    def test_init_app(self):
        """
        Ensure that the `base.blueprint` is registered in Flask app.
        """
        new_app = flask.Flask(__name__)

        base.init_app(new_app)

        self.assertIn(base.blueprint, new_app.blueprints.values())
