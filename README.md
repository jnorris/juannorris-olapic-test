# Olapic App

A test for Data Engineering position at Olapic.

A detailed description of the test can be found
[here](./docs/test_description.md).

***

## Quick Start

```sh
# Clone the repo
cd ~/<YOUR_WORKSPACE>
git clone ssh://git@bitbucket.org/jnorris/juannorris-olapic-test.git olapic_app
cd olapic_app

# Create a python virtual environment
mkvirtualenv olapic_app -a $(pwd)
# Install dev requirements (which in turns installs application requirements)
pip install -r requirements_dev.txt

# Run the Flask development server
python manage.py runserver -d -r
```

## Development

The first step is to clone the repository.

```sh
cd ~/<YOUR_WORKSPACE>
git clone ssh://git@bitbucket.org/jnorris/juannorris-olapic-test.git olapic_app
cd olapic_app
```

Once the repository has been cloned, the applications needs to be configured/installed.

### Virtual Env

First of all, install and configure a virtual environment for the application.

A great tool for the job is **virtualenvwrapper** (although not entirely necessary).

```sh
# Installing virtualenvwrapper
sudo pip install virtualenvwrapper
```

Creating a virtual environment is as easy as:

```sh
# Create a virtualenv and add the ability to switch to the current directory when enabling it
mkvirtualenv olapic_app -a $(pwd)

# Once the virtualenv has been created, it can be enabled with:
workon olapic_app
```

While developing, make sure you have installed development requirements.

```sh
pip install -r requirements_dev.txt
```

### Creating the Database

To run the application you need to install PostgreSQL in your system (although, MySQL should also work).

```sh
# Install posgresql
brew install postgresql

# Run postgres
postgres -D /usr/local/var/postgres
```

If this steps don't work for you, visit [https://www.postgresql.org/docs/]().

Then, create the user and DB:

```sh
# Create user for the DB
createuser olapic_user -P

# Create DB
createdb olapic_db -O olapic_user
```

### Loading Initial Data

Load initial raw data from file.

```sh
# Make sure your PYTHONPATH contains the path to the location of olapic_app
export PYTHONPATH=$(pwd)

# Make sure the database URI is set up, or pass it as a parameter to the script with --db option
export OLAPIC_APP_DATABASE_URI='postgresql://olapic_user@localhost:5432/olapic_db'

# Run the importer
python olapic_app/scripts/importer/data_importer.py --uhp <PATH_TO_RAW_DATA_FILE>
```

The script will output the number of records that were loaded and the total elapsed time.

### Running the Application

```sh
# Run the Flask development server
python manage.py runserver
```

For more options, type:

```sh
python manage.py runserver -h

# For instance, debugger and reloader can be enable
python manage.py runserver -d -r
```

Start a Python shell in the context of the Flask application (with some handy defaults already imported):

```sh
python manage.py shell
```

### Coding Style

Using `pycodestyle` helps enforcing good coding style practices (following [PEP8](https://www.python.org/dev/peps/pep-0008/)).

```sh
# Run the tool in the entire application (or a folder of your choosing)
pycodestyle olapic_app
```

Additionally, `pylint` runs static analysis and code style checks. The tool is configurable by changing the **.pylintrc** file.

```sh
# Run the tool in the entire application (or a folder of your choosing)
pylint olapic_app
```

### Running Tests

Tests are run using `nose`.

While running `nose`, `coverage` is also executed, generating reports about code coverage.

Both tools are configurable, by editing the [setup.cfg](./setup.cfg) file.

A PostgreSQL database is required to run tests.

```sh
# Create a test database
createdb olapic_test -O olapic_user

# Run tests against the recently created 'olapic_test' database
export OLAPIC_TEST_DATABASE_URI='postgresql://olapic_user@localhost:5432/olapic_test'

# Run tests in the entire application (or a folder of your choosing).
# This will return coverage statistics and generate report files
nosetests tests
```

NOTE: Remember to add/modify tests when developing/changing a feature.

## Troubleshooting

### Application

1. If you get an `ImportError` while running the application or a script, make
sure the `PYTHONPATH` is correctly set up, it should contain the path to the
application's root:

    ```sh
    export PYTHONPATH=<PATH_TO_APP_ROOT>
    ```

2. If you get an `AttributeError: 'NoneType' object has no attribute
'drivername'`, make sure the `OLAPIC_APP_DATABASE_URI` environment variable is
correctly set up, for example:

    ```sh
    export OLAPIC_APP_DATABASE_URI='postgresql://olapic_user@localhost:5432/olapic_db'
    ```

3. If you get a `404` error while performing a request, make sure the
`SERVER_NAME` environment variable is correctly set up.

    For local development, do not set that environment variable and use the
    provided `runserver` command.

### PostgreSQL

1. If psycopg2 fails to install with the following error:

    `  error: command 'clang' failed with exit status 1`

    (in OSX) try installing the **Command Line Tools**

    ```sh
    xcode-select --install
    ```

    and then re-run psycopg2 installation.

### Tests

1. If you get an exception about DB URI when running tests, make sure the
`OLAPIC_TEST_DATABASE_URI` environment variable is correctly set up:

    ```sh
    export OLAPIC_TEST_DATABASE_URI='postgresql://olapic_user@localhost:5432/olapic_test'
    ```
