"""
Configuration file for Flask application.
"""

import os

# Not entirely happy with importing application code into a config file, but
# this is Flask-RESTful's 'clean' way of using a custom JSONEncoder
from olapic_app.web.api.representations import custom_json_encoder


# Enables the development environment
DEBUG = os.getenv('DEBUG', False)

SERVER_NAME = os.getenv('SERVER_NAME', None)

# Secret key for signing cookies, should be randomly generated
SECRET_KEY = os.getenv('SECRET_KEY')

# For example: mysql+mysqldb://<user>:<password>@localhost:<port>/olapic_db or
# postgresql://<user>:<password>@localhost:<port>/olapic_db
SQLALCHEMY_DATABASE_URI = os.getenv('OLAPIC_APP_DATABASE_URI')

SQLALCHEMY_TRACK_MODIFICATIONS = False

# Flask-RESTful
ERROR_404_HELP = False
RESTFUL_JSON = {
    'indent': 2,
    'cls': custom_json_encoder.CustomJSONEncoder
}
