"""
Script to import initial data.

USAGE: `python data_importer.py --help`.
"""

import argparse
import logging
import os
import sys
from datetime import datetime
import pathlib2

from dateutil import parser as datetime_parser
from sqlalchemy import exc

from olapic_app.core import db
from olapic_app.core.user_hits import UserHit


class DataImporter(object):
    """
    Connects to a DB, parses and imports raw data.

    :ivar db_uri: A database URI.
    :ivar engine: An SQLAlchemy engine.
    :ivar connection: An SQLAlchemy connection.
    :ivar logger: A logger instance for the importer.
    """

    db_uri = ''
    engine = None
    connection = None
    logger = None

    def __init__(self, db_uri=None, user_hits_path=None):
        """
        Initializes DataImporter.

        If a `db_uri` is not provided, the value from the env variable
        `OLAPIC_APP_DATABASE_URI` would be used.

        :param db_uri: A database URI.
        :param user_hits_path: The absolute path to user hits raw data.
        """
        # Initialize a logger
        logging.basicConfig()
        self.logger = logging.getLogger('Importer')
        self.logger.setLevel(logging.INFO)

        if not user_hits_path:
            self.logger.error(
                'You need to provide an absolute path to the file containing'
                ' the user hits'
            )
            raise ValueError('User hits file path was not provided')

        self.file = pathlib2.Path(user_hits_path)

        if not self.file.is_file():
            self.logger.error('File "%s" does not exist.', user_hits_path)
            raise ValueError('Provided file does not exist')

        self.db_uri = db_uri or os.getenv('OLAPIC_APP_DATABASE_URI')

        if not self.db_uri:
            raise ValueError('No DB URI has been provided')

        self.init_db()

    def connect(self):
        """Open DB connection."""
        if not self.connection:
            self.init_db()

        # Open connection
        if self.engine:
            self.connection = self.engine.connect()

    def discconect(self):
        """Close DB session and connection"""
        db.session.close()
        self.connection.close()

    def init_db(self):
        """Initialize the database."""

        self.engine = db.create_engine(self.db_uri)
        db.session = db.scoped_session(
            db.sessionmaker(autoflush=False, bind=self.engine)
        )

    def import_user_hits(self):
        """
        Import user hits from file with raw data.

        :param file_path: absolute path to the file with the raw data.
        :return:
        """
        # FIXME: For dev purposes.
        # TODO: Maybe implement migrations and run them before importing.
        # Drop UserHit table
        UserHit.metadata.drop_all(self.engine)
        # Create UserHit table
        UserHit.metadata.create_all(self.engine)

        # Read file lines
        with self.file.open() as user_hits_file:
            for line in user_hits_file.readlines():
                line = line.strip()

                user_hit_data = line.split('|')

                if len(user_hit_data) != 5:
                    self.logger.warning(
                        'Skipping line due to missing data:\n%s',
                        user_hit_data
                    )
                    continue

                # Parse timestamp
                try:
                    timestamp = datetime_parser.parse(user_hit_data[1])
                except ValueError:
                    self.logger.warning(
                        'Failed to parse timestamp for line:\n%s',
                        user_hit_data
                    )
                    timestamp = None

                # Add user hit to the DB
                new_user = UserHit(
                    customer_id=user_hit_data[0],
                    timestamp=timestamp,
                    user_id=user_hit_data[2],
                    event_id=user_hit_data[3],
                    device_type=user_hit_data[4]
                )
                db.session.add(new_user)

    def import_data(self):
        """Imports raw data into the DB."""

        start_time = datetime.now()

        # TODO: While committing here is faster than doing it after each entry
        # TODO: is created, if there are any errors, they will be visible here,
        # TODO: after some minutes, so it might be worthwhile checking a better
        # TODO: way of doing this
        try:
            self.connect()
            self.import_user_hits()

            # Commit DB changes
            db.session.commit()
        except (KeyboardInterrupt, exc.DBAPIError) as ex:
            db.session.rollback()

            self.logger.warning('Failed to import data')
            self.logger.error(ex)
            sys.exit(1)
        else:
            self.logger.info(
                'Successfully imported %s actions',
                UserHit.query.count()
            )
        finally:
            # Close session and connection
            db.session.close()
            self.connection.close()

            self.logger.info('Elapsed time: %s', datetime.now() - start_time)


def parse_args():
    """
    Parse arguments to be used in the script.
    """
    parser = argparse.ArgumentParser(
        description='Load initial data'
    )

    parser.add_argument(
        '--db',
        '-database_uri',
        type=str,
        help='Database URI (e.g.: )'
    )

    parser.add_argument(
        '--uhp',
        '-user_hits_path',
        type=str,
        help='Absolute file path to user hits raw data',
        required=True
    )

    args = parser.parse_args()

    return args.db, args.uhp


def main():
    """Script main function"""
    db_uri, user_hits_path = parse_args()

    # Run data importer
    importer = DataImporter(db_uri, user_hits_path)
    importer.import_data()


if __name__ == "__main__":
    main()
