"""
Web application.
"""

from flask import Flask

from olapic_app.core import db
from olapic_app.web import api


BLUEPRINTS = [
    api
]


def create_app(config):
    """
    Create a Flask app and register existing blueprints on it.

    This function allows to create instances of the Flask application with
    different configurations attached, making unittests easier.

    :param config: config object.
    :return: WSGI application, a Flask object.
    """
    app = Flask(__name__)

    # Load default application settings
    app.config.from_object(config)

    # Initialize DB
    db.init_app(app)

    # Initialize all blueprints
    for blueprint in BLUEPRINTS:
        try:
            init_app = blueprint.init_app
        except AttributeError:
            raise AttributeError('%r has no init_app' % (blueprint.__name__,))

        init_app(app)

    return app
