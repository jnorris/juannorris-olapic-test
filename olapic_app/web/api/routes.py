"""
Resources routing definition.
"""

from olapic_app.web.api import user_hits
from olapic_app.web.api.base import restful_api


#
# Add resources routing
#
restful_api.add_resource(
    user_hits.CustomerConversionFunnel,
    '/customers/<int:customer_id>/conversion_funnel'
)
