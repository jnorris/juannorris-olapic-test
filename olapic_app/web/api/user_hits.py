"""
User hits resource module.

CustomerConversionFunnel:
    Handle conversion funnel metrics by customer.
"""

from flask_restful import Resource
from webargs import ValidationError, fields
from webargs.flaskparser import use_kwargs
from werkzeug.exceptions import NotFound
from olapic_app.core import user_hits


def customer_exists(customer_id):
    """
    Check if a customer exists by ID.

    :param customer_id: a customer ID.
    :raise NotFound: if the customer is not found.
    """
    # The customer should exist
    if not user_hits.customer_exists(customer_id):
        raise NotFound(description='Customer does not exist')


# TODO: Move this to a 'validators' or 'parser' module?
def validate_dates(parsed_args):
    """
    Validate that `date_from` is before than `date_to`, if both are present.

    :param parsed_args: a dictionary of parsed arguments.
    :return: True if `date_from` is before `date_to`.
    :raise ValidationError: if the `date_from` is equals or after `date_to`.
    """
    date_from = parsed_args.get('date_from', None)
    date_to = parsed_args.get('date_to', None)

    if date_from and date_to and date_from >= date_to:
        raise ValidationError(
            '\'date_from\' must be before \'date_to\''
        )

    return True


class CustomerConversionFunnel(Resource):
    """
    Handle conversion funnel metrics by customer.
    """

    # TODO: Investigate further on how this can be defined elsewhere and reused
    # Request parser for datetime parameters
    date_args = {
        'date_from': fields.DateTime(missing=None),  # noqa pylint: disable=no-member
        'date_to': fields.DateTime(missing=None)  # pylint: disable=no-member
    }

    @use_kwargs(date_args, validate=validate_dates)
    def get(self, customer_id, date_from, date_to):  # noqa pylint: disable=unused-argument,no-self-use
        """
        Return conversion funnel metrics by customer ID.

        :param customer_id: The id of the customer to obtain the metrics from.
        :return: a response containing the metrics for the given customer ID.
        """
        customer_exists(customer_id)

        conversion_funnel = user_hits.get_funnel_by_customer(
            customer_id,
            date_from=date_from,
            date_to=date_to
        )

        return {
            'data': {
                'customer': customer_id,
                'conversion_funnel': conversion_funnel
            }
        }
