"""
API package.
"""

# Make sure resources are loaded before the api gets initialized
from olapic_app.web.api import routes
from olapic_app.web.api.base import init_app
