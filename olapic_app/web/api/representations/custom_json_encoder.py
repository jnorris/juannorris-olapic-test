"""
JSON representation utils.
"""

from datetime import datetime

# Borrow function to convert to timestamp from python-dateutil
from dateutil.tz.tz import _datetime_to_timestamp as to_timestamp
from flask import json


class CustomJSONEncoder(json.JSONEncoder):
    """
    Custom `JSONEncoder`.

    Handle encoding of objects outside the JSON specification (like datetimes).
    """

    def default(self, obj):  # pylint: disable=method-hidden

        if isinstance(obj, datetime):
            # Obtain the datetime's timestamp
            encoded = to_timestamp(obj)
        else:
            # Use the default encoder
            encoded = json.JSONEncoder.default(self, obj)

        return encoded
