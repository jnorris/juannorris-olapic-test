"""
API main module.

api
    A `restful.Api` instance.
"""

import flask_restful as restful
from flask import blueprints

# from olapic_app.web.api import resources


# Create a blueprint for the API and the API itself
blueprint = blueprints.Blueprint(  # pylint: disable=invalid-name
    'olapic_app.api',
    __name__,
    url_prefix='/api/v1'
)

restful_api = restful.Api(blueprint)  # pylint: disable=invalid-name


def init_app(app):
    """
    Register `olapic_app.api` blueprint.

    :param app: Flask application.
    """

    app.register_blueprint(blueprint)
