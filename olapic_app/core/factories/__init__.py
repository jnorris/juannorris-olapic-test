"""
Factory boy configuration for testing purposes.
"""

import logging


# Factory-boy is quite verbose by default
logging.getLogger('factory').setLevel(logging.WARN)
