"""
User Hits factory module.

UserHitFactory
    A factory class to create user hits.
"""

from datetime import datetime, timedelta

import factory
from factory import fuzzy
import pytz

from olapic_app.core import user_hits, config
from olapic_app.core.factories.base import BaseFactory


class UserHitFactory(BaseFactory):
    """
    `UserHit` model factory.
    """

    customer_id = fuzzy.FuzzyInteger(1)
    timestamp = fuzzy.FuzzyDateTime(
        # Star date for the range is a year in the past
        (datetime.utcnow() - timedelta(days=365)).replace(tzinfo=pytz.utc)
    )
    user_id = factory.Sequence(lambda n: n + 1)
    event_id = fuzzy.FuzzyChoice(config.EVENT_TYPES)
    device_type = fuzzy.FuzzyInteger(1)

    # Auto generated when saved.
    # id

    class Meta(object):  # pylint: disable=missing-docstring
        model = user_hits.UserHit
