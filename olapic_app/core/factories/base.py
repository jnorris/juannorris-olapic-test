"""
Base factory.

BaseFactory
    Base factory model to avoid code repetition.
"""

import factory
from factory.alchemy import SQLAlchemyModelFactory

from tests.core.utils import get_db_session


class BaseFactory(SQLAlchemyModelFactory):
    """
    Base factory model to avoid code repetition.

    It has a scoped `SQLAlchemy` session.
    """

    __commit__ = True

    id = factory.Sequence(lambda n: n + 1)

    class Meta(object):  # pylint: disable=missing-docstring
        sqlalchemy_session = get_db_session()

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        """
        Create an instance of the model, and save it to the database.
        """
        session = cls._meta.sqlalchemy_session
        obj = model_class(*args, **kwargs)
        session.add(obj)

        if cls.__commit__:
            session.commit()

        return obj
