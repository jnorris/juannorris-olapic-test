"""
UserHit functions and models.

UserHit
    A UserHit DAO.
"""

from collections import OrderedDict
from datetime import datetime, timedelta

from olapic_app.core import config, utils
from olapic_app.core.base import db


class UserHit(db.Model):
    """
    UserHit DAO.

    Represents an action performed by a user.

    :ivar id: Internal primary key.
    :ivar customer_id: Customer's ID for this event.
    :ivar timestamp: When the event was captured.
    :ivar user_id: ID of the user whom performed the action.
    :ivar event_id: Event's id.
    :ivar device_type: Admin privileges.
    """

    __table_args__ = (
        db.Index('event_idx', 'event_id'),
        db.Index('user_customer_idx', 'user_id', 'customer_id'),
    )

    id = db.Column(db.Integer, primary_key=True)
    customer_id = db.Column(db.BigInteger, nullable=False)
    timestamp = db.Column(
        db.UTCDateTime, nullable=False, default=datetime.utcnow
    )
    user_id = db.Column(db.String(128), nullable=False)
    event_id = db.Column(db.Integer, nullable=False)
    device_type = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return '<UserHit: {}, {}>'.format(
            self.user_id,
            self.event_name
        )

    @property
    def event_name(self):
        """
        Return the event display name.

        :return: The event name, or None if `event_id` isn't in `EVENT_TYPES`.
        """
        return config.get_event_type_display(self.event_id)


def customer_exists(customer_id):
    """
    Check if a customer exists by ID.

    :param customer_id: a customer ID.
    :return: a boolean indicating whether the customer exists or not.
    """
    query = UserHit.query.filter(UserHit.customer_id == customer_id)

    return query.first() is not None


def get_events_expression(user_hit, step):
    """
    Create an SQLAlchemy `BinaryExpression` that checks if a `UserHit.event_id`
    belongs to a given step.

    The resulting binary expression will be either a comparison or an IN
    operation, depending on whether the given step is composed of 1 or more
    events, respectively. This makes it easy to map new events to steps.

    :param user_hit: `UserHit` class or an `AliasedClass` for `UserHit`.
    :param step: one of `config.STEPS`.
    :return: SQLAlchemy `BinaryExpression` for `UserHit.event_id`.
    :raise: `ValueError` when the step is not valid.
    """
    events = config.STEP_TO_EVENTS.get(step, None)

    if not events:
        raise ValueError('Invalid step')

    # Create the binary expression regardless of the number of events for the
    # given step
    if len(events) == 1:
        binary_expression = user_hit.event_id == events[0]
    else:
        binary_expression = user_hit.event_id.in_(events)

    return binary_expression


def get_funnel_by_customer(customer_id, date_from=None, date_to=None):
    """
    Return the number of users that completed each step sequentially, from
    render to checkout.

    Steps are defined in order in `config.STEPS`.

    :param customer_id: a customer ID.
    :param date_from: optional start datetime.
    :param date_to: optional end datetime.
    :return: a dictionary with step names as keys and numbers of users as
        values.
    """
    # If there are no steps, stop computation and return an empty dictionary
    if not len(config.STEPS):
        return {}

    first_step = config.STEPS[0]

    query = UserHit.query.filter(
        # Only the given customer is of interest
        UserHit.customer_id == customer_id,
        # Get all users that have completed the first step
        get_events_expression(UserHit, first_step)
    )

    # If date times are available, add the filters (also support dates)
    if date_from:
        date_from = utils.date_to_datetime(date_from)

        query = query.filter(date_from <= UserHit.timestamp)

    if date_to:
        date_to = utils.date_to_datetime(date_to)

        # Make it a parameter, so it can be reused in joins
        date_to = db.bindparam('date_to', date_to, db.DateTime())

        query = query.filter(UserHit.timestamp < date_to)

    # Count the number of users in the first step
    counts_by_step = [
        db.func.count(db.distinct(UserHit.user_id)).label(
            first_step
        )
    ]

    # Time window INTERVAL
    time_window = db.bindparam(
        'interval',
        timedelta(seconds=config.TIME_WINDOW),
        db.DateTime()
    )

    # Create an aliased table for each step after the first one
    tables_and_steps = [
        (db.aliased(UserHit), step)
        for step in config.STEPS[1:]
    ]

    # Perform joins in order, to calculate the funnel
    left_table = UserHit

    for right_table, step in tables_and_steps:
        counts_by_step.append(
            # Count the numbers of users in the current step
            db.func.count(db.distinct(right_table.user_id)).label(step)
        )

        # Make sure stepN is counted iff it has occurred after stepN-1 in the
        # pre-defined time window (for N > 1): stepN.timestamp between
        # [stepN-1.timestamp, stepN-1.timestamp + TIME_WINDOW]
        on_clause = db.and_(
            left_table.user_id == right_table.user_id,
            right_table.timestamp.between(
                left_table.timestamp,
                left_table.timestamp + time_window
            ),
            get_events_expression(right_table, step)
        )

        # Ensure the upper bound datetime is fulfilled (lower bound is ensured
        # by the previous step)
        if date_to is not None:
            on_clause = db.and_(on_clause, right_table.timestamp < date_to)

        # Apply the join with the current step
        query = query.outerjoin(right_table, on_clause)

        left_table = right_table

    # Count each user only once per step
    query = query.with_entities(*counts_by_step)

    # Execute the final query
    result = db.session.execute(query).first()

    # Make sure a funnel object is always returned, in the pre-defined order
    if result:
        funnel = OrderedDict(result.items())
    else:
        funnel = OrderedDict(
            [(step, 0) for step in config.STEPS]
        )

    return funnel
