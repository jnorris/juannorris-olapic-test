"""
Core config.
"""

from collections import OrderedDict


#
# Events constants
#
EVENT_CHECKOUT = 1
EVENT_CHECKOUT_NAME = 'Checkout'
EVENT_RENDER = 11
EVENT_RENDER_NAME = 'Widget Render'
EVENT_MOVE = 13
EVENT_MOVE_NAME = 'Widget Move'
EVENT_PHOTO_UPLOAD = 14
EVENT_PHOTO_UPLOAD_NAME = 'Photo Upload'
EVENT_PHOTO_CLICK = 15
EVENT_PHOTO_CLICK_NAME = 'Photo Click'
# Categorize all uninteresting events
EVENT_DEFAULT = -1
EVENT_DEFAULT_NAME = 'Other'

EVENT_TYPES = OrderedDict([
    (EVENT_DEFAULT, EVENT_DEFAULT_NAME),
    # Step1
    (EVENT_RENDER, EVENT_RENDER_NAME),
    # Step2
    (EVENT_MOVE, EVENT_MOVE_NAME),
    (EVENT_PHOTO_UPLOAD, EVENT_PHOTO_UPLOAD_NAME),
    (EVENT_PHOTO_CLICK, EVENT_PHOTO_CLICK_NAME),
    # Step3
    (EVENT_CHECKOUT, EVENT_CHECKOUT_NAME)
])

INTERESTING_EVENTS = EVENT_TYPES.keys()
INTERESTING_EVENTS.remove(EVENT_DEFAULT)

#
# Steps constants
#
STEP_VIEW = 'step_1'
STEP_INTERACTION = 'step_2'
STEP_CHECKOUT = 'step_3'

# Define a mapping from steps to events
STEP_TO_EVENTS = OrderedDict([
    # Step 1
    (STEP_VIEW, [
        EVENT_RENDER
    ]),
    # Step 2
    (STEP_INTERACTION, [
        EVENT_MOVE,
        EVENT_PHOTO_UPLOAD,
        EVENT_PHOTO_CLICK
    ]),
    # Step 3
    (STEP_CHECKOUT, [
        EVENT_CHECKOUT
    ])
])

# Steps in order
STEPS = STEP_TO_EVENTS.keys()

# TODO: Maybe obtain this from an environment variable
# Time window between each step: 30 minutes (expressed in seconds)
TIME_WINDOW = 30 * 60


def get_event_type_display(event_type):
    """
    Return the display name for a given `event_type`.

    :param event_type: the event type.
    :return: A display name for the given `event_type`, if it exists,
        otherwise, the default: `EVENT_DEFAULT_NAME`.
    """

    return EVENT_TYPES.get(event_type, EVENT_DEFAULT_NAME)
