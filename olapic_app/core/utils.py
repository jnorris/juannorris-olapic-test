"""
Utility functions.
"""

from datetime import date, datetime

import pytz


def datetime_to_naive(aware_dt):
    """
    Returns the naive version of the given (aware) datetime object.

    :param aware_dt: an aware datetime object.
    :return: a naive datetime object.
    """
    try:
        naive_dt = aware_dt.astimezone(pytz.utc).replace(tzinfo=None)
    except ValueError:
        # The datetime was already naive
        naive_dt = aware_dt.replace(tzinfo=None)

    return naive_dt


def date_to_datetime(obj):
    """
    Return a datetime object from a date object.

    If the object is anything else than a date, return it.

    :return: `datetime` if the object is either `date` or `datetime`, the same
    object otherwise.
    """
    # Only convert date objects to datetime
    if not isinstance(obj, datetime) and isinstance(obj, date):
        obj = datetime(obj.year, obj.month, obj.day)

    return obj
