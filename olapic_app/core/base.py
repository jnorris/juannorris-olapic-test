"""
SQLAlchemy DB declaration.

db
    Provides access to all the SQLAlchemy functions and classes from the
    :mod:`sqlalchemy` and :mod:`sqlalchemy.orm` modules.

UTCDateTime
    A `TypeDecorator` to handle aware datetime objects (in UTC).

BaseModel
    A base abstract declarative model that provides some common columns and
    methods for subclasses (declarative models).
"""

from datetime import datetime
import pytz

from flask_sqlalchemy import Model, SQLAlchemy
from sqlalchemy import TypeDecorator, Column, DateTime

from olapic_app.core import utils


class UTCDateTime(TypeDecorator):  # pylint: disable=abstract-method
    """
    Extends DateTime type to handle aware datetime objects (in UTC).

    Converts from aware datetime to naive (in UTC) before saving to the DB, and
    converts naive datetime to UTC aware datetime after reading from the DB.
    """

    impl = DateTime

    def process_bind_param(self, value, dialect):  # noqa pylint: disable=unused-argument,no-self-use
        """
        Converts from aware datetime to naive (in UTC) before saving to the DB.

        :param value: value to be saved to the DB.
        :param dialect: an SQLAlchemy Dialect.
        :return: the parsed value, or None.
        """
        if value and isinstance(value, datetime):
            return utils.datetime_to_naive(value)

    def process_result_value(self, value, dialect):  # noqa pylint: disable=unused-argument,no-self-use
        """
        Converts naive datetime to UTC aware datetime after reading from DB.

        :param value: value read from the DB.
        :param dialect: an SQLAlchemy Dialect.
        :return: the parsed value, or None.
        """
        if isinstance(value, datetime):
            return value.replace(tzinfo=pytz.utc)


class BaseModel(Model):
    """
    Base abstract declarative model that provides some common columns and
    methods for subclasses.

    :ivar created_on: Creation datetime.
    :ivar last_modified: Last modification datetime.
    """

    __abstract__ = True

    created_on = Column(
        UTCDateTime,
        default=datetime.utcnow
    )
    last_modified = Column(
        UTCDateTime,
        default=datetime.utcnow,
        onupdate=datetime.utcnow
    )

    def __repr__(self):
        return unicode(self.__dict__)

    def to_json(self):
        """
        Return a dict that is suitable for JSON encoding.
        """
        # Get the mapper object, to be able to iterate over the columns
        mapper = self.__class__.__mapper__

        ret = {}

        for name, _ in mapper.column_attrs.items():
            value = getattr(self, name)

            # Display datetime in ISO format
            if isinstance(value, datetime):
                value = value.isoformat()

            ret[name] = value

        return ret


# Replace `SQLAlchemy.Model` base declarative model with a new base model
db = SQLAlchemy(  # noqa pylint: disable=invalid-name
    model_class=BaseModel,
    session_options={'autoflush': False}
)
# Make classes available on the db object
db.UTCDateTime = UTCDateTime
