"""
SQLAlchemy DB declaration.

db
    Provides access to all the SQLAlchemy functions and classes from the
    :mod:`sqlalchemy` and :mod:`sqlalchemy.orm` modules.
"""

# Make db object and models available at package level
from olapic_app.core.base import db
from olapic_app.core.user_hits import UserHit
